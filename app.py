# -*- coding: utf-8 -*-
import flask, shelve
from datetime import datetime
from flask import url_for, request
from flask import render_template
__author__ = ["Júnior Mário", "github.com/JuniorMario"]
PORT = "5000"
HOST = "0.0.0.0"
data = "{}-{}".format(str(datetime.now).split(" ")[0], str(datetime.now()).split(" ")[1].split(".")[0])
db = shelve.open("db/posts.db")
app = flask.Flask(__name__)
options = ["oi", "tim", "claro", "vivo"]
def load():
    global db
    contatos = []
    for i in list(db.keys()):
        contatos.append(db[i])
    return contatos
@app.route("/posta", methods=['POST'])
def posta():
        dic = {"name": request.form["nome"], "telefone":request.form["telefone"], "operadora":request.form["operadora"], "data":data}
        db[request.form["telefone"]] = dic
        print("inside", dic)
        return flask.redirect("/post", code=302)
@app.route("/")
def index():
    return render_template("post.html", filename=app.name, port=8000, contatos=["oi"], options=options)
@app.route("/post")
def post():
    contatos = load()
    return render_template("post.html", filename=app.name, port=PORT, contatos=contatos, options=options)

if __name__ == '__main__':
    app.run(debug=True, host=HOST)
